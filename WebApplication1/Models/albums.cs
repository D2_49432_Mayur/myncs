﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class albums
    {
        public int albumid { get; set; }
        public string albumname { get; set; }
        public string albumimage { get; set; }
        public int artistid { get; set; }
    }
}
