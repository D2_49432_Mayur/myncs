﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class songs
    {
        public int songid { get; set; }
        public string songname { get; set; }
        public string songimage { get; set; }
        public int albumid { get; set; }
        public string songfilename { get; set; }
    }
}
