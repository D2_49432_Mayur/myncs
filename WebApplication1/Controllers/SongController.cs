﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SongController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public SongController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            string query = @"
                select * from songs
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MyNCSAppCon");
            NpgsqlDataReader myReader;
            using(NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using(NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            List < songs > songList = new();

            foreach (DataRow row in table.Rows)
            {
                songs song = new songs();
                
            }
            

            return Ok(table);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne(int id)
        {
            string query = @"
                select * from songs where songid = @songid
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MyNCSAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@songid", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            if (table.Rows.Count == 0)
                return NotFound();

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(songs song)
        {
            string query = @"
                INSERT INTO songs(songname,songimage,albumid,songfilename) VALUES(@songname,@songimage,@albumid,@songfilename)
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MyNCSAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@songname",song.songname);
                    myCommand.Parameters.AddWithValue("@songimage", song.songimage);
                    myCommand.Parameters.AddWithValue("@albumid", song.albumid);
                    myCommand.Parameters.AddWithValue("@songfilename", song.songfilename);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Added Successfully");
        }

        [HttpPut]
        public JsonResult Put(songs song)
        {
            string query = @"
                UPDATE songs  SET songname = @songname ,songimage = @songimage ,albumid = @albumid ,songfilename=@songfilename  where songid = @songid
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MyNCSAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@songname", song.songname);
                    myCommand.Parameters.AddWithValue("@songimage", song.songimage);
                    myCommand.Parameters.AddWithValue("@albumid", song.albumid);
                    myCommand.Parameters.AddWithValue("@songfilename", song.songfilename);
                    myCommand.Parameters.AddWithValue("@songid", song.songid);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                DELETE FROM songs WHERE
                songid = @songid
            ";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("MyNCSAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@songid", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Deleted Successfully");
        }

    }
}
