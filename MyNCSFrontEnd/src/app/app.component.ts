import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MyNCS';
  songState = "Play"
  songName = "Are_You_Ok_1.mp3"
  song = new Audio("assets/songs/"+this.songName);

  togglePlay(){
    if(this.songState=="Play")
      { this.song.play();
        this.songState="Pause"
      }
    else{
      this.songState="Play"
      this.song.pause();
    }
  }
  
}
