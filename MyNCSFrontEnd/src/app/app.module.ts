import { Inject, Injectable, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LibraryComponent } from './library/library.component';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from '@angular/common/http'

@NgModule({
  declarations: [
    AppComponent,
    LibraryComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
export class Song{
  public songid:number;
  public songimage:string;
  public songname:string;
  public albumid:number;
  public songfilename:string;

  constructor(  songid:number,
    songimage:string,
    songname:string,
    albumid:number,
    songfilename:string){
      this.songid=songid;
      this.albumid=albumid;
      this.songfilename=songfilename;
      this.songimage=songimage;
      this.songname=songname;
  }
}