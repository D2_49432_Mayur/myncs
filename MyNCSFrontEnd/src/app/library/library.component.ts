import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Song } from '../app.module';






@Component({
  selector: 'app-library',
  template: `
    <p>
      library works!
      <table border="1">
        <thead>
          <td>song id</td>
          <td>song image</td>
          <td>song name</td>
          <td>album id</td>
          <td>songfilename</td>
        </thead>
        <tbody>
          <tr *ngFor="let song of songs">
          <td>song id</td>
          <td>song image</td>
          <td>song name</td>
          <td>album id</td>
          <td>songfilename</td>
        </tbody>
      </table>
    
  `,
  styles: [
    `table {color:azure;
      border: solid;
      border-color: azure;
    }`
  ]
})



export class LibraryComponent implements OnInit {


  songs:Song[]

  constructor(
    private httpClient: HttpClient

  ) { }

  ngOnInit(): void {

    this.httpClient.get<any>('http://localhost:8080/getallsongs').subscribe(
      response => {
          console.log(response)  
          this.songs = response      
       }
    )

  }

}
